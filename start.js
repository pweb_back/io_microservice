const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const createError = require('http-errors');
const cors = require('cors');
const { auth } = require('express-oauth2-jwt-bearer');

require('express-async-errors');
require('log-timestamp');

const routes = require('./routes.js');

const app = express();

app.use(helmet());
app.use(morgan(':remote-addr - :remote-user [:date[web]] ":method :url HTTP/:http-version" :status :res[content-length]'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const frontendOrigin = process.env.FE_ORIGIN || 'http://localhost:4200';

app.use(cors({
    origin: frontendOrigin
}));

const isJwtVerificationEnabled = process.env.VERIFY_JWT === 'true';

const checkJwt = auth({
    audience: 'https://dev-vlziad43.eu.auth0.com/api/v2/',
    issuerBaseURL: `https://dev-vlziad43.eu.auth0.com/`,
});

if (isJwtVerificationEnabled) {
    app.use('/api', checkJwt, routes);
} else {
    app.use('/api', routes);
}

app.use((err, req, res, next) => {
    if (!!err && 'status' in err) {
        return next(err);
    }

    console.error(err);
    let status = 500;
    let message = 'Something Bad Happened';
    return next(createError(status, message));
});

const port = process.env.PORT || 80;

app.listen(port, () => {
    console.log(`App is listening on ${port}`);
});
