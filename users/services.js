const {
  query,
} = require('../data');

const getUsers = async () => {
    console.info(`Getting all users ...`);

    const users = await query("SELECT * FROM users");

    return users;
};

const getUserById = async (id) => {
    console.info(`Getting user ${id} ...`);

    const users = await query("SELECT user_id, username, picture, email, userType, is_verified, is_enabled FROM users WHERE user_id = $1", [id]);
    //Todo : mapat campurile conform interfetei descrise de Silviu
    return users[0];
};

const addUser = async (id, username, picture, email, type, isUserVerified, isEnabled) => {
    console.info(`Adding user with user id ${id}, username ${username}, picture ${picture}, email ${email},
     type ${type}, isUserVerified ${isUserVerified}, isEnabled ${isEnabled} ...`);

    const users = await query("INSERT INTO users (user_id, username, picture, email, userType, is_verified, is_enabled) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING user_id",
        [id, username, picture, email, type, isUserVerified, isEnabled]);

    return users[0].id;
};

const updateUser = async (id, username, picture, email) => {
    console.info(`Updating user with user id ${id}, username ${username}, picture ${picture}, email ${email}`);

    const user = await query("UPDATE users SET username = $2, picture = $3, email = $4 WHERE user_id = $1",
        [id, username, picture, email]);
    return user;
};

const changeUserRole = async (id, newRole) => {
    console.info(`Changing is_enabled of user with user id ${id}, to type ${newRole}`);

    const user = await query("UPDATE users SET userType = $2 WHERE user_id = $1",
        [id, newRole]);

    return user;
};

const changeUserEnabled = async (id, enabled) => {
    console.info(`Changing is_enabled of user with user id ${id}, to type ${enabled}`);

    const user = await query("UPDATE users SET is_enabled = $2 WHERE user_id = $1",
        [id, enabled]);

    return user;
};

const changeUserVerified = async (id, verified) => {
    console.info(`Changing is_verified of user with user id ${id}, to ${verified}`);

    const user = await query("UPDATE users SET is_verified = $2 WHERE user_id = $1",
        [id, verified]);

    return user;
};


module.exports = {
    getUsers,
    getUserById,
    addUser,
    changeUserRole,
    updateUser,
    changeUserEnabled,
    changeUserVerified
};
