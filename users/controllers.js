const Router = require('express').Router();

const {
    getUsers,
    getUserById,
    addUser,
    changeUserRole,
    updateUser,
    changeUserEnabled,
    changeUserVerified
} = require('./services.js');

Router.get('/', async (req, res) => {

    const users = await getUsers();

    res.json(users);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;
    console.log(id)
    const user = await getUserById(id);

    res.json(user);
});

Router.post('/', async (req, res) => {
    const {
        id,
        username,
        picture,
        email,
        type,
        isUserVerified,
        isEnabled
    } = req.body;

    const user_id = await addUser(id, username, picture, email, type, isUserVerified, isEnabled);

    res.json(user_id);
});

Router.put('/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const {
        username,
        picture,
        email
    } = req.body;

    const user = await updateUser(id, username, picture, email);

    res.json(user);
});

Router.post('/:id/change-role/:role', async (req, res) => {
    const { id, role } = req.params;

    await changeUserRole(id, role);

    res.json(null);
});

Router.post('/:id/change-enabled/:isEnabled', async (req, res) => {
    const { id, isEnabled } = req.params;

    await changeUserEnabled(id, isEnabled);

    res.json(null);
});

Router.post('/:id/change-verified/:isVerified', async (req, res) => {
    const { id, isVerified } = req.params;

    await changeUserVerified(id, isVerified);

    res.json(null);
});


module.exports = Router;
