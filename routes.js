const Router = require('express').Router();

const UsersController = require('./users/controllers.js');
const PostsController = require('./posts/controllers.js');

Router.use('/users', UsersController);
Router.use('/posts', PostsController);

module.exports = Router;