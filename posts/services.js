const {
    query
} = require('../data');

const getPosts = async () => {
    console.info(`Getting all posts ...`);

    const posts = await query("SELECT * FROM posts");

    return posts;
};

const getPostById = async (post_id) => {
    console.info(`Getting post with post_id ${post_id} ...`);

    const posts = await query("SELECT * FROM posts WHERE post_id = $1", [post_id]);

    return posts[0];
};

const addPost = async (user_id, postType, category, title, description, image,
                       upvotes, upvoted, downvoted, interactionCount, isRetired, tags) => {
    console.info(`Adding post of user ${user_id}, postType ${postType}, category ${category},
      title ${title}, description ${description}, image ${image}, upvotes ${upvotes}, downvoted ${downvoted},
       upvoted ${upvoted}, interactionCount ${interactionCount}, isRetired ${isRetired}, tags ${tags}...`);
    const post = await query("INSERT INTO posts (user_id, postType, category, title, description, image," +
        "upvotes, upvoted, downvoted, interactionCount, isRetired, tags) " +
        "VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
        [user_id, postType, category, title, description, image,
            upvotes, upvoted, downvoted, interactionCount, isRetired, tags]);

};

const addTags = async (post_id, tags) => {
    console.info(`Adding tags ${tags} to post ${post_id}`);
    const posts = await query("UPDATE posts SET tags = array_append((SELECT tags FROM posts WHERE post_id = $1), $2) WHERE post_id = $1",
        [post_id, tags]);
};

const removeTag = async (post_id, tag) => {
    console.info(`Adding tags ${tags} to post ${post_id}`);
    const posts = await query("UPDATE posts SET tags = array_remove((SELECT tags FROM posts WHERE post_id = $1), $2) WHERE post_id = $1",
        [post_id, tag]);
};

//TODO!!!
const upvote = async (post_id) => {
    const post = await getPostById(post_id)
    console.log(`Post : ${post}`)
    let isUpvoted = post.upvoted;
    let isDownvoted = post.downvoted;
    let score = post.upvotes;
    console.info(`Upvoting post with id ${post_id}, isUpvoted ${isUpvoted}, isDownvoted, ${isDownvoted}, score ${score}...`)
    if (isUpvoted) {
        // is upvoted -> toggle upvote, -1 to score
        if (isDownvoted) {
            console.log("ERROR! BOTH UPVOTED AND DOWNVOTED POST!")
        } else {
            score -= 1;
            isUpvoted = !isUpvoted;
            console.info(`Upvoting post with id ${post_id}, isUpvoted ${isUpvoted}, isDownvoted, ${isDownvoted}, score ${score}...`)
            await query("UPDATE posts SET upvoted = $2, downvoted = $3, upvotes = $4 WHERE post_id = $1",[post_id, isUpvoted, isDownvoted, score]);
        }
    } else {
        if (isDownvoted) {
            //downvoted -> uncheck downvote, check upvote, +2 to score
            isUpvoted = true;
            isDownvoted = false;
            score += 2;
            console.info(`Upvoting post with id ${post_id}, isUpvoted ${isUpvoted}, isDownvoted, ${isDownvoted}, score ${score}...`)
            await query("UPDATE posts SET upvoted = $2, downvoted = $3, upvotes = $4 WHERE post_id = $1",[post_id, isUpvoted, isDownvoted, score]);
        }
        else {
            // neither upvoted nor downvoted -> upvote, +1 to score
            isUpvoted = true;
            score += 1;
            console.info(`Upvoting post with id ${post_id}, isUpvoted ${isUpvoted}, isDownvoted, ${isDownvoted}, score ${score}...`)
            await query("UPDATE posts SET upvoted = $2, downvoted = $3, upvotes = $4 WHERE post_id = $1",[post_id, isUpvoted, isDownvoted, score]);
        }
    }
};
//TODO!!!
const downvote = async (post_id, tags) => {
    const post = await getPostById(post_id)
    let isUpvoted = post.upvoted;
    let isDownvoted = post.downvoted;
    let score = post.upvotes;
    console.info(`Downvoting post with id ${post_id}...`)
    if (isUpvoted) {
        if (isDownvoted) {
            console.log("ERROR! BOTH UPVOTED AND DOWNVOTED POST!")
        } else {
            // is upvoted -> toggle upvote, -1 to score
            isUpvoted = false;
            isDownvoted = true;
            score -= 2;
            await query("UPDATE posts SET upvoted = $2, downvoted = $3, upvotes = $4 WHERE post_id = $1",[post_id, isUpvoted, isDownvoted, score]);
        }
    } else {
        if (isDownvoted) {
            //downvoted -> uncheck downvote, +1 to score
            isDownvoted = false;
            score += 1;
            await query("UPDATE posts SET upvoted = $2, downvoted = $3, upvotes = $4 WHERE post_id = $1",[post_id, isUpvoted, isDownvoted, score]);
        }
        else {
            // neither upvoted nor downvoted -> downvote, -1 to score
            isDownvoted = true;
            score -= 1;
            await query("UPDATE posts SET upvoted = $2, downvoted = $3, upvotes = $4 WHERE post_id = $1",[post_id, isUpvoted, isDownvoted, score]);
        }
    }
};

const interact = async (post_id, user_id, text, phone) => {
    console.info(`Adding interaction ${text}, ${phone} to post ${post_id}`);
    console.log("User id : ", user_id)
    const users = await query("SELECT * FROM users WHERE user_id = $1", [user_id])
    console.log("Query result : ", users)
    await query("INSERT INTO interactions (post_id, text, phone, email, posterName) VALUES ($1, $2, $3, $4, $5)",
        [post_id, text, phone, users[0].email, users[0].user_id])

};

const retirePost = async (post_id) => {
    console.info(`Retiring post ${post_id}`);
    const posts = await query("UPDATE posts SET isRetired = true WHERE post_id = $1",
        [post_id]);
};

const deletePost = async (post_id) => {
    console.info(`Deleting post ${post_id}`);
    const posts = await query("DELETE FROM posts WHERE post_id = $1",
        [post_id]);
};

const editPostDescription = async (post_id, description) => {
    console.info(`For post ${post_id} setting new description : ${description}`);
    const posts = await query("UPDATE posts SET description = $2 WHERE post_id = $1",
        [post_id, description]);
};

const getLatestPost = async () => {
    console.info(`Getting latest post ...`);
    // sort posts in descending order of post_id and get the first one
    const posts = await query("SELECT * FROM posts ORDER BY post_id DESC LIMIT 1");

    return posts[0];
};

const getUserPosts = async (user_id) => {
    console.info(`Getting posts of user ${user_id} ...`);

    const posts = await query("SELECT * FROM posts WHERE user_id = $1", [user_id]);
    for (const item of posts) {
        const post_id = item.post_id;
        item.interactions = await query("SELECT * FROM interactions WHERE post_id = $1", [post_id])
    }
    return posts;
};


module.exports = {
    getPosts,
    getPostById,
    addPost,
    addTags,
    removeTag,
    upvote,
    downvote,
    interact,
    retirePost,
    deletePost,
    editPostDescription,
    getLatestPost,
    getUserPosts
}