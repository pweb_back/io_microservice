const Router = require('express').Router();

const {
    getPosts,
    getPostById,
    addPost,
    addTags,
    removeTag,
    upvote,
    downvote,
    interact,
    retirePost,
    deletePost,
    editPostDescription,
    getLatestPost,
    getUserPosts
} = require('./services.js');

Router.get('/', async (req, res) => {
    
    const posts = await getPosts();

    res.json(posts);
});

Router.get('/latest', async (req, res) => {

    const post = await getLatestPost();

    res.json(post);
});

Router.get('/get-user-posts/:id', async (req, res) => {
    const {
        id
    } = req.params;

    const post = await getUserPosts(id);

    res.json(post);
});

Router.get('/:id', async (req, res) => {

    const {
        id
    } = req.params;

    const post = await getPostById(id);

    res.json(post);
});

Router.delete('/:id', async (req, res) => {

    const {
        id
    } = req.params;

    const post = await deletePost(id);

    res.json(post);
});

Router.post('/', async (req, res) => {
    //Todo : modificat format date
    const {
        user_id,
        postType,
        category,
        title,
        description,
        image,
        upvotes,
        upvoted,
        downvoted,
        interactionCount,
        isRetired,
        tags
    } = req.body;

    const id = await addPost(user_id, postType, category, title, description, image,
        upvotes, upvoted, downvoted, interactionCount, isRetired, tags);

    res.json(id);
});

Router.post('/:id/add-tags', async (req, res) => {
    const {
        id
    } = req.params;

    const {
        tags
    } = req.body;

    const post = await addTags(id, tags);

    res.json(post);
});

Router.post('/:id/remove-tag', async (req, res) => {
    const {
        id
    } = req.params;

    const {
        tag
    } = req.body;

    const post = await removeTag(id, tag);

    res.json(null);
});

Router.post('/:id/upvote', async (req, res) => {
    const {
        id
    } = req.params;

    const post = await upvote(id);

    res.json(null);
});

Router.post('/:id/downvote', async (req, res) => {
    const {
        id
    } = req.params;

    const post = await downvote(id);

    res.json(null);
});

Router.post('/:id/retire', async (req, res) => {
    const {
        id
    } = req.params;

    const post = await retirePost(id);

    res.json(post);
});

Router.post('/:id/edit-description', async (req, res) => {
    console.log("///////////////////////")
    console.log(req.body, "\n---------------\n", req.body)
    const {
        id
    } = req.params;

    const {
        description
    } = req.body;

    const post = await editPostDescription(id, description);

    res.json(post);
});

Router.post('/:id/interact', async (req, res) => {
    const {
        id,
    } = req.params;

    const {
        user_id,
        text,
        phone
    } = req.body;

    const post = await interact(id, user_id, text, phone);

    res.json(post);
});

module.exports = Router;